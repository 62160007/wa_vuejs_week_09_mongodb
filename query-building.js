const mongoose = require('mongoose')
const Room = require('./models/Room')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  // update
  // const room = await Room.findById('6219f16b62b38a3bd6b36f1a')
  // room.capacity = 20
  // room.save()
  // console.log(room)

  // const room = await Room.findOne({ _id: '6219f16b62b38a3bd6b36f1a' })
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('--------------------------')

  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(building))
}

main().then(() =>
  console.log('Finish'))
